#!/usr/bin/python3

import argparse
import re
import subprocess

parser = argparse.ArgumentParser()

parser.add_argument(
	'-r', '--rotate',
	action='store_true'
)

parser.add_argument(
	'-o', '--output',
	default='HDMI-1',
)

parser.add_argument(
	'-p', '--pressure',
	default='15 55 65 85'
)

args = parser.parse_args()

reg = re.compile('^(.*)\tid: (\d+)\ttype: (\w+)')
def map_data(raw):
	m = reg.match(raw)
	return {
		'name': m.group(1).strip(),
		'id': m.group(2),
		'type': m.group(3).strip()
	}


def find(pred, items):
	for x in items:
		if pred(x):
			return x


devices = str(subprocess.run(['xsetwacom', 'list'], check=True, capture_output=True).stdout, 'utf-8').split('\n')
devices = [map_data(d) for d in devices if d]

print('Found the following inputs:')
for d in devices:
	print('\t{id:5} {name:40} {type}'.format(**d))


if args.rotate:
	for d in devices:
		subprocess.run(['xsetwacom', 'set', d['id'], 'Rotate', 'half'], check=True)

for d in devices:
	subprocess.run(['xsetwacom', 'set', d['id'], 'MapToOutput', args.output])

def apply_pressure(device):
	return device['type'] in [ 'STYLUS', 'ERASER' ]

if args.pressure:
	for d in [ d for d in devices if apply_pressure(d) ]:
		subprocess.run([ 'xsetwacom', 'set', d['id'], 'PressureCurve', args.pressure])
	

print('Setup complete.')
